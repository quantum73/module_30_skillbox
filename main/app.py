import os.path
from datetime import datetime

from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy

ROOT_DIR = os.path.abspath(os.getcwd())
DB_PATH = os.path.join(ROOT_DIR, "module29.sqlite")
db = SQLAlchemy()


def create_app():
    app = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = f"sqlite:////{DB_PATH}"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    db.init_app(app)

    from main.models import Client, Parking, ClientParking

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        db.session.remove()

    @app.route("/clients/", methods=["GET"])
    @app.route("/clients/<int:client_id>/", methods=["GET"])
    def get_clients(client_id: int = None):
        if not client_id:
            clients = [c.to_json() for c in Client.query.all()]
            return jsonify(clients), 200

        client_obj = Client.query.get(client_id)
        if client_obj:
            return jsonify(client_obj.to_json()), 200
        else:
            return jsonify(message="client not found"), 404

    @app.route("/clients/", methods=["POST"])
    def create_client():
        client_data = request.json
        try:
            client_obj = Client(**client_data)
        except TypeError as e:
            return jsonify(error=str(e)), 400

        db.session.add(client_obj)
        db.session.commit()

        return jsonify(client_obj.to_json()), 201

    @app.route("/parkings/", methods=["POST"])
    def create_parking():
        parking_data = request.json
        try:
            parking_obj = Parking(**parking_data)
        except TypeError as e:
            return jsonify(error=str(e)), 400

        db.session.add(parking_obj)
        db.session.commit()

        return jsonify(parking_obj.to_json()), 201

    @app.route("/client_parkings/", methods=["POST", "DELETE"])
    def create_client_parking():
        client_parking_data = request.json
        client_id = client_parking_data.get("client_id")
        parking_id = client_parking_data.get("parking_id")
        if not all((client_id, parking_id)):
            return jsonify(error="you passed invalid data"), 404

        client_obj = Client.query.filter_by(id=client_id).first()
        parking_obj = Parking.query.filter_by(id=parking_id).first()
        if not client_obj:
            return jsonify(message="client not found"), 404
        if not parking_obj:
            return jsonify(message="parking not found"), 404

        if request.method == "DELETE":
            client_parking_obj = ClientParking.query.filter_by(
                client_id=client_id, parking_id=parking_id
            ).first()
            if not client_parking_obj:
                return jsonify(message="client_parking not found"), 404
            if not client_obj.credit_card:
                return jsonify(message="you do not have a credit card"), 400

            new_count_available_places = parking_obj.count_available_places + 1
            Parking.query.filter_by(id=parking_id).update(
                dict(count_available_places=new_count_available_places)
            )
            ClientParking.query.filter_by(
                client_id=client_id, parking_id=parking_id
            ).update(dict(time_out=datetime.now()))
            db.session.commit()

            return jsonify(client_parking_obj.to_json()), 200
        else:
            if not client_obj.car_number:
                return jsonify(message="you do not have a car"), 400
            if not parking_obj.opened:
                return jsonify(message="parking is closed"), 400
            if parking_obj.count_available_places == 0:
                return jsonify(message="there are no parking places"), 400

            new_count_available_places = parking_obj.count_available_places - 1
            Parking.query.filter_by(id=parking_id).update(
                dict(count_available_places=new_count_available_places)
            )

            client_parking_obj = ClientParking(
                client_id=client_id,
                parking_id=parking_id,
                time_in=datetime.now(),
            )
            db.session.add(client_parking_obj)
            db.session.commit()
            return jsonify(client_parking_obj.to_json()), 201

    return app
