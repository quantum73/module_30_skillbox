from .app import db
from typing import Dict, Any


class ToJSONMixin:
    def to_json(self) -> Dict[str, Any]:
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Client(db.Model, ToJSONMixin):
    __tablename__ = "client"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    surname = db.Column(db.String(50), nullable=False)
    credit_card = db.Column(db.String(50), nullable=True)
    car_number = db.Column(db.String(10), nullable=True)
    parking = db.relationship("ClientParking", back_populates="clients")

    def __repr__(self):
        return f"{self.name} {self.surname}"


class Parking(db.Model, ToJSONMixin):
    __tablename__ = "parking"

    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(100), nullable=False)
    opened = db.Column(db.Boolean, default=True)
    count_places = db.Column(db.Integer, nullable=False)
    count_available_places = db.Column(db.Integer, nullable=False)
    clients = db.relationship("ClientParking", back_populates="parking")

    def __repr__(self):
        return f"Парковка ({self.address})"


class ClientParking(db.Model, ToJSONMixin):
    __tablename__ = "client_parking"

    id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(db.Integer, db.ForeignKey("client.id"), unique=True)
    parking_id = db.Column(db.Integer, db.ForeignKey("parking.id"), unique=True)
    parking = db.relationship("Parking", back_populates="clients")
    clients = db.relationship("Client", back_populates="parking")
    time_in = db.Column(db.DateTime(timezone=True), nullable=True)
    time_out = db.Column(db.DateTime(timezone=True), nullable=True)
    db.ForeignKeyConstraint(
        ("client_id", "parking_id"),
        ("client.id", "parking.id"),
        name="unique_client_parking",
    )
    db.UniqueConstraint("unique_client_parking")
