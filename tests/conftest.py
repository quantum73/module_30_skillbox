import random

import pytest

from main.app import create_app, db as _db
from main.models import Client, Parking


@pytest.fixture
def client_with_valid_data():
    client_data = {
        "name": "Ivan",
        "surname": "Ivanov",
        "car_number": "252",
        "credit_card": "134235",
    }
    return client_data


@pytest.fixture
def parking_with_valid_data():
    parking_data = {
        "address": "Some address",
        "opened": True,
        "count_places": 100,
        "count_available_places": 100,
    }
    return parking_data


@pytest.fixture
def random_client():
    client = {
        "name": f"Name {random.randint(100, 999)}",
        "surname": f"Surname {random.randint(100, 999)}",
        "credit_card": str(random.randint(100, 999))
        if random.choice((True, False))
        else None,
        "car_number": str(random.randint(100, 999))
        if random.choice((True, False))
        else None,
    }
    return client


@pytest.fixture
def random_parking():
    places = random.randint(100, 999)
    parking = {
        "address": f"Address {random.randint(100, 999)}",
        "opened": random.choice((True, False)),
        "count_places": places,
        "count_available_places": places,
    }
    return parking


@pytest.fixture
def app():
    _app = create_app()
    _app.config["TESTING"] = True
    _app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

    with _app.app_context():
        _db.create_all()

        clients = []
        parkings = []
        for idx in range(1, 11):
            client = {
                "name": f"Name {idx}",
                "surname": f"Surname {idx}",
                "credit_card": random.randint(100, 999)
                if random.choice((True, False))
                else None,
                "car_number": random.randint(100, 999)
                if random.choice((True, False))
                else None,
            }
            places = random.randint(100, 999)
            parking = {
                "address": f"Address {idx}",
                "opened": random.choice((True, False)),
                "count_places": places,
                "count_available_places": places,
            }
            clients.append(Client(**client))
            parkings.append(Parking(**parking))

        _db.session.add_all(clients)
        _db.session.add_all(parkings)

        _db.session.commit()

        yield _app
        _db.session.close()
        _db.drop_all()


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


@pytest.fixture
def db(app):
    with app.app_context():
        yield _db
