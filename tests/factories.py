import factory
import factory.fuzzy as fuzzy
import random

from main.app import db
from main.models import Client, Parking


class ClientFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Client
        sqlalchemy_session = db.session

    name = factory.Faker("first_name")
    surname = factory.Faker("last_name")
    car_number = factory.LazyAttribute(
        lambda x: str(random.randint(100, 999)) if random.choice((0, 1)) else None
    )
    credit_card = factory.LazyAttribute(
        lambda x: str(random.randint(100, 999)) if random.choice((0, 1)) else None
    )


class ParkingFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Parking
        sqlalchemy_session = db.session

    address = factory.Faker("address")
    opened = factory.LazyAttribute(lambda x: random.choice([True, False]))
    count_places = fuzzy.FuzzyInteger(50, 200)
    count_available_places = factory.LazyAttribute(lambda obj: obj.count_places)
