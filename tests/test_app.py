from datetime import datetime

import pytest

from main.models import Parking


@pytest.mark.parametrize("route", ["/clients/", "/clients/1/"])
def test_all_get_methods(client, route) -> None:
    resp = client.get(route)
    assert resp.status_code == 200


def test_create_client(client, random_client) -> None:
    client_data = random_client
    resp = client.post("/clients/", json=client_data)
    assert resp.status_code == 201

    response_data = resp.json
    assert response_data.get("id") is not None

    response_data.pop("id")
    assert client_data == response_data


def test_create_parking(client, random_parking) -> None:
    parking_data = random_parking
    resp = client.post("/parkings/", json=parking_data)
    assert resp.status_code == 201

    response_data = resp.json
    assert response_data.get("id") is not None

    response_data.pop("id")
    assert parking_data == response_data


@pytest.mark.parking
def test_get_in_parking(
    client, db, client_with_valid_data, parking_with_valid_data
) -> None:
    client_resp = client.post("/clients/", json=client_with_valid_data)
    parking_resp = client.post("/parkings/", json=parking_with_valid_data)
    count_available_places = parking_with_valid_data.get("count_available_places")
    client_id = client_resp.json.get("id")
    parking_id = parking_resp.json.get("id")

    now_datetime = datetime.now().strftime("%a, %d %b %Y %H:%M:%S %ZGMT")
    client_parkings_obj = {
        "client_id": client_id,
        "parking_id": parking_id,
    }
    client_parking_resp = client.post("/client_parkings/", json=client_parkings_obj)
    assert client_parking_resp.status_code == 201

    client_parking_data = client_parking_resp.json
    assert client_parking_data.get("id") is not None
    assert client_parking_data.get("client_id") == client_id
    assert client_parking_data.get("parking_id") == parking_id
    assert client_parking_data.get("time_in") == now_datetime
    assert client_parking_data.get("time_out") is None

    parking_obj = db.session.query(Parking).get(parking_id)
    assert parking_obj.count_available_places == count_available_places - 1


@pytest.mark.parking
def test_get_out_parking(
    client, db, client_with_valid_data, parking_with_valid_data
) -> None:
    client_resp = client.post("/clients/", json=client_with_valid_data)
    parking_resp = client.post("/parkings/", json=parking_with_valid_data)
    count_available_places = parking_with_valid_data.get("count_available_places")
    client_id = client_resp.json.get("id")
    parking_id = parking_resp.json.get("id")
    client_parkings_obj = {
        "client_id": client_id,
        "parking_id": parking_id,
    }
    client.post("/client_parkings/", json=client_parkings_obj)

    now_datetime = datetime.now().strftime("%a, %d %b %Y %H:%M:%S %ZGMT")
    client_parking_resp = client.delete("/client_parkings/", json=client_parkings_obj)
    assert client_parking_resp.status_code == 200

    client_parking_data = client_parking_resp.json
    assert client_parking_data.get("time_out") == now_datetime

    parking_obj = db.session.query(Parking).get(parking_id)
    assert parking_obj.count_available_places == count_available_places
