from main.models import Client, Parking
from tests.factories import ClientFactory, ParkingFactory


def test_create_client(app, db):
    client = ClientFactory()
    db.session.commit()
    assert client.id is not None
    assert len(db.session.query(Client).all()) == 11


def test_create_parking(client, db):
    parking = ParkingFactory()
    db.session.commit()
    print(parking.address)
    assert parking.id is not None
    assert len(db.session.query(Parking).all()) == 11
